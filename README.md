# Boîte à outils accessibilité

Mes techniques, astuces et recommandations d'accessibilité (a11y), en français.

Consulter le wiki pour des [spécifications pour du code <span lang="en">front-end</span> accessible](https://gitlab.com/accessfirst.fr/boite-outils-accessibilite/-/wikis/Accueil/Recommandations-g%C3%A9n%C3%A9rales).